/*
 * catmouse.c
 *
 * 30-1-2003 : GWA : Stub functions created for CS161 Asst1.
 * 26-11-2007: KMS : Modified to use cat_eat and mouse_eat
 * 21-04-2009: KMS : modified to use cat_sleep and mouse_sleep
 * 21-04-2009: KMS : added sem_destroy of CatMouseWait
 *
 */


/*
 * 
 * Includes
 *
 */

#include <types.h>
#include <lib.h>
#include <test.h>
#include <thread.h>
#include <synch.h>
#include "opt-A1.h"

/*
 * 
 * cat,mouse,bowl simulation functions defined in bowls.c
 *
 * For Assignment 1, you should use these functions to
 *  make your cats and mice eat from the bowls.
 * 
 * You may *not* modify these functions in any way.
 * They are implemented in a separate file (bowls.c) to remind
 * you that you should not change them.
 *
 * For information about the behaviour and return values
 *  of these functions, see bowls.c
 *
 */

/* this must be called before any calls to cat_eat or mouse_eat */
extern int initialize_bowls(unsigned int bowlcount);

extern void cat_eat(unsigned int bowlnumber);
extern void mouse_eat(unsigned int bowlnumber);
extern void cat_sleep(void);
extern void mouse_sleep(void);

/*
 *
 * Problem parameters
 *
 * Values for these parameters are set by the main driver
 *  function, catmouse(), based on the problem parameters
 *  that are passed in from the kernel menu command or
 *  kernel command line.
 *
 * Once they have been set, you probably shouldn't be
 *  changing them.
 *
 *
 */
int NumBowls;  // number of food bowls
int NumCats;   // number of cats
int NumMice;   // number of mice
int NumLoops;  // number of times each cat and mouse should eat

// Global variables needed for concurrency
#if OPT_A1
  /* Bowl semaphore used for making sure that the number of cats/mice trying to find a bowl does not exceed
     the number of bowls. You can think of it as a queue which leads up the bowl area, and the bowl area only
     holds NumBowls cats/mice at any time */
  struct semaphore *BowlsSemaphore;

  /* Array of locks for the bowls. Used to make sure only one cat/mouse is eating from a specific
     bowl at any given time */
  struct lock **BowlLocks;

  /* Used soley to count the number of cats/mice that are at a bowl and aren't done eating yet */
  struct semaphore *CatsLeftEatingSemaphore;
  struct semaphore *MiceLeftEatingSemaphore;

  /* The two CVs that allow cats and mice to alternate, Cats will cv_wait on CatsTurnCV until the last mouse to
     eat calls cv_broadcast on CatsTurnCV and wakes them up, and vice versa */
  struct cv *CatTurnCV;
  struct cv *MiceTurnCV;
  // The locks used for the two CVs above respectively
  struct lock *CatTurnLock;
  struct lock *MiceTurnLock;

  /* The types of turns. It's either the cats' turn to eat, or the mice's, or we haven't chosen yet */
  enum turns {
      UNDECIDED,
      CATS,
      MICE
  };
  // The current turn
  enum turns Turn;
  // The lock used for deciding the turn
  struct lock *TurnLock;
#endif /* OPT_A1 */

/*
 * Once the main driver function (catmouse()) has created the cat and mouse
 * simulation threads, it uses this semaphore to block until all of the
 * cat and mouse simulations are finished.
 */
struct semaphore *CatMouseWait;

/*
 * 
 * Function Definitions
 * 
 */

/* Switches to the cat's turn */
void catTurn() {
  Turn = CATS;
  int j;
  for (j = 0; j< NumBowls; j++) {
    V(BowlsSemaphore);
  }
  lock_acquire(CatTurnLock);
    cv_broadcast(CatTurnCV,CatTurnLock);
  lock_release(CatTurnLock);
}

/* Switches to the mouse turn */
void mouseTurn() {
  Turn = MICE;
  int j;
  for (j = 0; j< NumBowls; j++) {
    V(BowlsSemaphore);
  }
  lock_acquire(MiceTurnLock);
    cv_broadcast(MiceTurnCV,MiceTurnLock);
  lock_release(MiceTurnLock);
}

/*
 * cat_simulation()
 *
 * Arguments:
 *      void * unusedpointer: currently unused.
 *      unsigned long catnumber: holds cat identifier from 0 to NumCats-1.
 *
 * Returns:
 *      nothing.
 *
 * Notes:
 *      Each cat simulation thread runs this function.
 *
 *      You need to finish implementing this function using 
 *      the OS161 synchronization primitives as indicated
 *      in the assignment description
 */

static
void
cat_simulation(void * unusedpointer, 
	       unsigned long catnumber)
{
  int i;
  unsigned int bowl;

  /* avoid unused variable warnings. */
  (void) unusedpointer;
  (void) catnumber;

  /* your simulated cat must iterate NumLoops times,
   *  sleeping (by calling cat_sleep() and eating
   *  (by calling cat_eat()) on each iteration */

  #if OPT_A1
    lock_acquire(TurnLock);
      if (Turn == UNDECIDED) {
        Turn = CATS;
      }
    lock_release(TurnLock);
  #endif /* OPT_A1 */

  for(i=0;i<NumLoops;i++) {

    /* do not synchronize calls to cat_sleep().
       Any number of cats (and mice) should be able
       sleep at the same time. */
    cat_sleep();

    #if OPT_A1

      /* Take control of the turn even if it is the mice's turn IF there are no mice running */
      if (Turn != CATS) {
        lock_acquire(TurnLock);
          if (MiceLeftEatingSemaphore->count == 0) {
            Turn = CATS;
          }
        lock_release(TurnLock);
      }

      /* Queue this animal up for a bowl, the animal will shortly lose it's position if it's not this animal type's turn */
      P(BowlsSemaphore);

      /* Wait for the mice to stop running (they will broadcast to us) */
      if (Turn != CATS) {
        V(BowlsSemaphore);
        lock_acquire(CatTurnLock);
          cv_wait(CatTurnCV,CatTurnLock);
        lock_release(CatTurnLock);
        P(BowlsSemaphore);
      }
      V(CatsLeftEatingSemaphore);
        // Choose a random bowl to tentatively to eat from
        bowl = ((unsigned int)random() % NumBowls) + 1;

        lock_acquire(BowlLocks[bowl]);
          cat_eat(bowl);
        lock_release(BowlLocks[bowl]);

        /* If the number of cats that have eaten this turn (this cat being the last cat to eat) is equal to the
           number of bowls, we need to let the mice eat after this cat is done */
        if (BowlsSemaphore->count == 0 && CatsLeftEatingSemaphore->count == 1) {
          mouseTurn();
        }
      P(CatsLeftEatingSemaphore);
    #else
      /* for now, this cat chooses a random bowl from
       * which to eat, and it is not synchronized with
       * other cats and mice.
       *
       * you will probably want to control which bowl this
       * cat eats from, and you will need to provide 
       * synchronization so that the cat does not violate
       * the rules when it eats */

      /* legal bowl numbers range from 1 to NumBowls */
      bowl = ((unsigned int)random() % NumBowls) + 1;
      cat_eat(bowl);
    #endif /* OPT_A1 */  
  }
  /* indicate that this cat simulation is finished */
  V(CatMouseWait); 
}
	
/*
 * mouse_simulation()
 *
 * Arguments:
 *      void * unusedpointer: currently unused.
 *      unsigned long mousenumber: holds mouse identifier from 0 to NumMice-1.
 *
 * Returns:
 *      nothing.
 *
 * Notes:
 *      each mouse simulation thread runs this function
 *
 *      You need to finish implementing this function using 
 *      the OS161 synchronization primitives as indicated
 *      in the assignment description
 *
 */

static
void
mouse_simulation(void * unusedpointer,
          unsigned long mousenumber)
{
  int i;
  unsigned int bowl;

  /* avoid unused variable warnings. */
  (void) unusedpointer;
  (void) mousenumber;

  #if OPT_A1
    lock_acquire(TurnLock);
      if (Turn == UNDECIDED) {
        Turn = MICE;
      }
    lock_release(TurnLock);
  #endif

  /* your simulated mouse must iterate NumLoops times,
   *  sleeping (by calling mouse_sleep() and eating
   *  (by calling mouse_eat()) on each iteration */
  for(i=0;i<NumLoops;i++) {

    /* do not synchronize calls to mouse_sleep().
       Any number of cats (and mice) should be able
       sleep at the same time. */
    mouse_sleep();

    #if OPT_A1
      /* Take control of the turn even if it is the mice's turn IF there are no mice running */
      if (Turn != MICE) {
        lock_acquire(TurnLock);
          if (CatsLeftEatingSemaphore->count == 0) {
            Turn = MICE;
          }
        lock_release(TurnLock);
      }

      /* Queue this animal up for a bowl, the animal will shortly lose it's position if it's not this animal type's turn */
      P(BowlsSemaphore);

      /* Wait for the mice to stop running (they will broadcast to us) */
      if (Turn != MICE) {
        lock_acquire(TurnLock);
          if (CatsLeftEatingSemaphore->count == 0) {
            Turn = MICE;
          }
        lock_release(TurnLock);
      }

      if (Turn != MICE) {
        V(BowlsSemaphore);
        lock_acquire(MiceTurnLock);
          cv_wait(MiceTurnCV,MiceTurnLock);
        lock_release(MiceTurnLock);
        P(BowlsSemaphore);
      }
      V(MiceLeftEatingSemaphore);
        // Choose a random bowl to tentatively to eat from
        bowl = ((unsigned int)random() % NumBowls) + 1;

        lock_acquire(BowlLocks[bowl]);
          mouse_eat(bowl);
        lock_release(BowlLocks[bowl]);

        /* If the number of mice that have eaten this turn (this mouse being the last mouse to eat) is equal
           to the number of bowls, we need to let the cats eat after this mouse is done */
        if (BowlsSemaphore->count == 0 && MiceLeftEatingSemaphore->count == 1) {
          catTurn();
        }
      P(MiceLeftEatingSemaphore);
    #else
      /* for now, this mouse chooses a random bowl from
       * which to eat, and it is not synchronized with
       * other cats and mice.
       *
       * you will probably want to control which bowl this
       * mouse eats from, and you will need to provide 
       * synchronization so that the cat does not violate
       * the rules when it eats */

      /* legal bowl numbers range from 1 to NumBowls */
      bowl = ((unsigned int)random() % NumBowls) + 1;
      mouse_eat(bowl);
    #endif /* OPT_A1 */  
  }
  /* indicate that this mouse simulation is finished */
  V(CatMouseWait); 
}


/*
 * catmouse()
 *
 * Arguments:
 *      int nargs: should be 5
 *      char ** args: args[1] = number of food bowls
 *                    args[2] = number of cats
 *                    args[3] = number of mice
 *                    args[4] = number of loops
 *
 * Returns:
 *      0 on success.
 *
 * Notes:
 *      Driver code to start up cat_simulation() and
 *      mouse_simulation() threads.
 *      You may need to modify this function, e.g., to
 *      initialize synchronization primitives used
 *      by the cat and mouse threads.
 *      
 *      However, you should should ensure that this function
 *      continues to create the appropriate numbers of
 *      cat and mouse threads, to initialize the simulation,
 *      and to wait for all cats and mice to finish.
 */

int
catmouse(int nargs,
	 char ** args)
{
  int index, error;
  int i;

  /* check and process command line arguments */
  if (nargs != 5) {
    kprintf("Usage: <command> NUM_BOWLS NUM_CATS NUM_MICE NUM_LOOPS\n");
    return 1;  // return failure indication
  }

  /* check the problem parameters, and set the global variables */
  NumBowls = atoi(args[1]);
  if (NumBowls <= 0) {
    kprintf("catmouse: invalid number of bowls: %d\n",NumBowls);
    return 1;
  }
  NumCats = atoi(args[2]);
  if (NumCats < 0) {
    kprintf("catmouse: invalid number of cats: %d\n",NumCats);
    return 1;
  }
  NumMice = atoi(args[3]);
  if (NumMice < 0) {
    kprintf("catmouse: invalid number of mice: %d\n",NumMice);
    return 1;
  }
  NumLoops = atoi(args[4]);
  if (NumLoops <= 0) {
    kprintf("catmouse: invalid number of loops: %d\n",NumLoops);
    return 1;
  }
  kprintf("Using %d bowls, %d cats, and %d mice. Looping %d times.\n",
          NumBowls,NumCats,NumMice,NumLoops);

  /* create the semaphore that is used to make the main thread
     wait for all of the cats and mice to finish */
  CatMouseWait = sem_create("CatMouseWait",0);
  if (CatMouseWait == NULL) {
    panic("catmouse: could not create semaphore\n");
  }

  /* 
   * initialize the bowls
   */
  if (initialize_bowls(NumBowls)) {
    panic("catmouse: error initializing bowls.\n");
  }

  #if OPT_A1
    // Initialize synchronization primitives
    BowlsSemaphore = sem_create("Bowl Semaphore", NumBowls); 
    BowlLocks = kmalloc(NumBowls * (sizeof(struct lock *) + 1)); // +1 because bowl indices start at 1
    int j;
    for (j = 1; j <= NumBowls; j++) {
      BowlLocks[j] = lock_create("A Bowl Lock");
    }

    CatsLeftEatingSemaphore = sem_create("Cats Left Eating Semaphore", 0);
    MiceLeftEatingSemaphore = sem_create("Mice Left Eating Semaphore", 0);

    CatTurnCV = cv_create("Cat Turn CV");
    MiceTurnCV = cv_create("Mice Turn CV");

    CatTurnLock = lock_create("Cat Turn Lock");
    MiceTurnLock = lock_create("Mice Turn Lock");
    TurnLock = lock_create("Turn Selection Lock");

    Turn = UNDECIDED;
  #endif /* OPT_A1  */

  /*
   * Start NumCats cat_simulation() threads.
   */
  for (index = 0; index < NumCats; index++) {
    error = thread_fork("cat_simulation thread",NULL,index,cat_simulation,NULL);
    if (error) {
      panic("cat_simulation: thread_fork failed: %s\n", strerror(error));
    }
  }

  /*
   * Start NumMice mouse_simulation() threads.
   */
  for (index = 0; index < NumMice; index++) {
    error = thread_fork("mouse_simulation thread",NULL,index,mouse_simulation,NULL);
    if (error) {
      panic("mouse_simulation: thread_fork failed: %s\n",strerror(error));
    }
  }

  /* wait for all of the cats and mice to finish before
     terminating */  
  for(i=0;i<(NumCats+NumMice);i++) {
    P(CatMouseWait);
  }

  /* clean up the semaphore the we created */
  sem_destroy(CatMouseWait);
  #if OPT_A1
    // Clean up synchronization primitives
    sem_destroy(BowlsSemaphore);
    sem_destroy(CatsLeftEatingSemaphore);
    sem_destroy(MiceLeftEatingSemaphore);
    lock_destroy(CatTurnLock);
    lock_destroy(MiceTurnLock);
    lock_destroy(TurnLock);
    cv_destroy(MiceTurnCV);
    cv_destroy(CatTurnCV);
    for (j = 1; j <= NumBowls;j++) {
      lock_destroy(BowlLocks[j]);
    }
    kfree(BowlLocks);
  #endif /* OPT_A1  */

  return 0;
}

/*
 * End of catmouse.c
 */
