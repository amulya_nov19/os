/*
 * Synchronization primitives.
 * See synch.h for specifications of the functions.
 */

#include <types.h>
#include <lib.h>
#include <synch.h>
#include <thread.h>
#include <curthread.h>
#include <machine/spl.h>
#include "opt-A1.h"

////////////////////////////////////////////////////////////
//
// Semaphore.

struct semaphore *
sem_create(const char *namearg, int initial_count)
{
	struct semaphore *sem;

	assert(initial_count >= 0);

	sem = kmalloc(sizeof(struct semaphore));
	if (sem == NULL) {
		return NULL;
	}

	sem->name = kstrdup(namearg);
	if (sem->name == NULL) {
		kfree(sem);
		return NULL;
	}

	sem->count = initial_count;
	return sem;
}

void
sem_destroy(struct semaphore *sem)
{
	int spl;
	assert(sem != NULL);

	spl = splhigh();
	assert(thread_hassleepers(sem)==0);
	splx(spl);

	/*
	 * Note: while someone could theoretically start sleeping on
	 * the semaphore after the above test but before we free it,
	 * if they're going to do that, they can just as easily wait
	 * a bit and start sleeping on the semaphore after it's been
	 * freed. Consequently, there's not a whole lot of point in 
	 * including the kfrees in the splhigh block, so we don't.
	 */

	kfree(sem->name);
	kfree(sem);
}

void 
P(struct semaphore *sem)
{
	int spl;
	assert(sem != NULL);

	/*
	 * May not block in an interrupt handler.
	 *
	 * For robustness, always check, even if we can actually
	 * complete the P without blocking.
	 */
	assert(in_interrupt==0);

	spl = splhigh();
	while (sem->count==0) {
		thread_sleep(sem);
	}
	assert(sem->count>0);
	sem->count--;
	splx(spl);
}

void
V(struct semaphore *sem)
{
	int spl;
	assert(sem != NULL);
	spl = splhigh();
	sem->count++;
	assert(sem->count>0);
	thread_wakeup(sem);
	splx(spl);
}

////////////////////////////////////////////////////////////
//
// Lock.

struct lock *
lock_create(const char *name)
{
	struct lock *lock;

	lock = kmalloc(sizeof(struct lock));
	if (lock == NULL) {
		return NULL;
	}

	lock->name = kstrdup(name);
	if (lock->name == NULL) {
		kfree(lock);
		return NULL;
	}
	
  #if OPT_A1
    lock->inUse = 0;
  #endif /* OPT_A1 */
	
	return lock;
}

void
lock_destroy(struct lock *lock)
{
	assert(lock != NULL);

  #if OPT_A1
    int spl = splhigh();
    assert(thread_hassleepers(lock)==0);
    splx(spl);
  #endif /* OPT_A1 */

	kfree(lock->name);
	kfree(lock);
}

void
lock_acquire(struct lock *lock)
{
  #if OPT_A1
  	assert(lock != NULL);
    assert(!lock_do_i_hold(lock));
  	assert(in_interrupt==0);
	  int spl;
	  spl = splhigh();
    while (lock->inUse)
      thread_sleep(lock);
    lock->inUse = 1;
    lock->owner = curthread;
    splx(spl);
  #else
	  (void)lock;  // suppress warning until code gets written
  #endif /* OPT_A1 */
}

void
lock_release(struct lock *lock)
{
  #if OPT_A1
    assert(lock != NULL);
    assert(curthread != NULL);
    assert(curthread == lock->owner);

    int spl = splhigh();
    lock->inUse = 0;
    lock->owner = NULL;
    thread_wakeup(lock);
    splx(spl);
  #else
	  (void)lock;  // suppress warning until code gets written
  #endif /* OPT_A1 */
}

int
lock_do_i_hold(struct lock *lock)
{
  #if OPT_A1
    return lock != NULL && lock->owner == curthread;
  #else
    (void)lock;  // suppress warning until code gets written
    return 1;    // dummy until code gets written
  #endif /* OPT_A1 */
}


////////////////////////////////////////////////////////////
//
// Lock Queue
#if OPT_A1
  void threadQueueInsert(struct threadQueue **threadQueue, struct thread *element) {
    struct threadQueue *newNode = kmalloc(sizeof(struct threadQueue));
    newNode->next = NULL;
    newNode->thread = element;
    if (*threadQueue == NULL) {
      *threadQueue = newNode;
    } else {
      struct threadQueue *ptr = *threadQueue;
      while (ptr->next != NULL)
        ptr = ptr->next;
      ptr->next = newNode;
    }
  }

  struct thread *threadQueueRemove(struct threadQueue **threadQueue) {
    assert(threadQueue != NULL);

    struct thread *firstThread = (*threadQueue)->thread;
    struct threadQueue *head = *threadQueue;
    *threadQueue = (*threadQueue)->next;
    kfree(head);
    return firstThread;
  }

  void threadQueueDelete(struct threadQueue *threadQueue) {
    if (threadQueue == NULL)
      return;
    struct threadQueue *next = threadQueue->next;
    kfree(threadQueue);
    threadQueueDelete(next);
  }
#endif /* OPT_A1 */

////////////////////////////////////////////////////////////
//
// CV

struct cv *
cv_create(const char *name)
{
	struct cv *cv;

	cv = kmalloc(sizeof(struct cv));
	if (cv == NULL) {
		return NULL;
	}

	cv->name = kstrdup(name);
	if (cv->name==NULL) {
		kfree(cv);
		return NULL;
	}
	
	// add stuff here as needed
  #if OPT_A1
    cv->threads = NULL;
  #endif /* OPT_A1 */
	
	return cv;
}

void
cv_destroy(struct cv *cv)
{
	assert(cv != NULL);

	// add stuff here as needed
  #if OPT_A1
    // Make sure there are no threads left waiting on this CV (otherwise they will become zombies)
    assert(cv->threads == NULL);
  #endif /* OPT_A1 */
	
	kfree(cv->name);
	kfree(cv);
}

void
cv_wait(struct cv *cv, struct lock *lock)
{
	// Write this
  #if OPT_A1
    assert(cv != NULL); 
    assert(lock != NULL); 
    assert(lock_do_i_hold(lock));

    int spl = splhigh();
    // Add the currently running thread to the wait list for the cv
    threadQueueInsert(&cv->threads, curthread);
    // Release the lock and then go to sleep to let some other thread do it's thing'
    lock_release(lock);
    thread_sleep(curthread); // Sleep the thread on it's own address

    // When we're woken up we need to grab the lock back
    lock_acquire(lock);
    assert(lock_do_i_hold(lock));
    splx(spl);
  #else
    (void)cv;    // suppress warning until code gets written
    (void)lock;  // suppress warning until code gets written
  #endif /* OPT_A1 */
}

void
cv_signal(struct cv *cv, struct lock *lock)
{
	// Write this
  #if OPT_A1
    assert(cv != NULL);
    assert(lock != NULL);
    assert(lock_do_i_hold(lock))

    // No thread to signal, do nothing
    if (cv->threads == NULL)
      return;

    int spl = splhigh();
    // Remove and then wake the lock at the front of the blocked threads queue
    struct thread *threadToActivate = threadQueueRemove(&(cv->threads));
    thread_wakeup(threadToActivate);

    // Release the lock so the woken thread can resume
    splx(spl);
  #else
    (void)cv;    // suppress warning until code gets written
    (void)lock;  // suppress warning until code gets written
  #endif /* OPT_A1 */
}

void
cv_broadcast(struct cv *cv, struct lock *lock)
{
	// Write this
  #if OPT_A1
    assert(cv != NULL);
    assert(lock != NULL);
    assert(lock_do_i_hold(lock));

    int spl = splhigh();
    // Wake all the blocked threads in the queue
    while (cv->threads != NULL) {
      struct thread *threadToActivate = threadQueueRemove(&(cv->threads));
      thread_wakeup(threadToActivate);
    }
    // Release the lock so that one of the blocked threads can continue
    splx(spl);
  #else
    (void)cv;    // suppress warning until code gets written
    (void)lock;  // suppress warning until code gets written
  #endif /* OPT_A1 */
}
